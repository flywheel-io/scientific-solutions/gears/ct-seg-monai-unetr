"""The fw_gear_ct_seg_monai_unetr package."""
from importlib.metadata import version

try:
    __version__ = version(__package__)
except:  # pragma: no cover
    pass
